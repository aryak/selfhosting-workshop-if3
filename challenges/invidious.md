# Invidious Setup

Invidious is a privacy-respecting YouTube frontend that minimizes the amount of data shared with Google for viewing videos.

- Clone the Invidious git repository with the necessary setup files: `git clone --depth=1 https://github.com/iv-org/invidious.git`

- The docker compose setup is defined in the `docker-compose.yml` file. It describes how various components such as the Postgres database and the Invidious backend should be set up.

- Modify the `docker-compose.yml` file to use a pre-built Invidious rather than building it from source:

```diff
--- a/docker-compose.yml
+++ b/docker-compose.yml
@@ -8,9 +8,7 @@ version: "3"
 services:
 
   invidious:
-    build:
-      context: .
-      dockerfile: docker/Dockerfile
+    image: quay.io/invidious/invidious:latest
     restart: unless-stopped
     ports:
       - "127.0.0.1:3000:3000"
```

- Change the value of `hmac_key` under `environment` to a secret value, it is used for creating tokens.

To generate HMAC Key: `pwgen 20 1`

- Additionally, we also need to set some things related to domain of the instance

```diff
--- a/docker-compose.yml
+++ b/docker-compose.yml
@@ -30,7 +28,7 @@ services:
-        # external_port:
+        external_port: 443
-        # domain:
+        domain: invidious.HOSTNAME_IN_CHIT
-        # https_only: false
+        https_only: true
         # statistics_enabled: false
-        hmac_key: "CHANGE_ME!!"
+        hmac_key: "KEY_GENERTED_WITH_PWGEN"
     healthcheck:
       test: wget -nv --tries=1 --spider http://127.0.0.1:3000/api/v1/comments/jNQXAC9IVRw || exit 1
       interval: 30s
```

- Provision the instance: `docker compose up -d`

  - The `-d` flag means "detach", which keeps the containers running in the background even when we exit our shell

---

## Reverse Proxy

Invidious is now listening on port `3000` on our VPS! We can now set up a caddy reverse proxy for it.

- Add the following contents to `/etc/caddy/Caddyfile`. This will forward all requests to the `invidious` subdomain to the running instance:

```
invidious.HOSTNAME_IN_CHIT {
  reverse_proxy localhost:3000
}
```

- Run `caddy reload` and visit Invidious in your browser!
