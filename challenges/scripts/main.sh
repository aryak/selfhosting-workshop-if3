#!/usr/bin/env bash

./center_print_ascii.sh ascii-art.txt && printf '\n'
./center_print_ascii.sh $(./leaderboard.sh > /tmp/leaderboard.sh; echo /tmp/leaderboard.sh) && printf '\n'
./center_print_ascii.sh welcome.txt

read -p "Enter your name (for leaderboard): " name
read -p "Choose which challenge you want to try (enter the number): " challenge

if [[ $challenge == "1" ]]; then
	echo "You have selected the Authoritative DNS Server setup challenge."
	echo "Provisioning VPS..."
	ssh if3@authdns.k8x.in "/usr/local/bin/reset-to-snapshot"
	sleep 5
	echo "Provisioned! Entering the Matrix..."
	echo "Please follow instruction booklet number 1"
	SECONDS=0 # bash builtin; see https://overflow.projectsegfau.lt/exchange/unix/questions/12068/how-to-measure-time-of-program-execution-and-store-that-inside-a-variable#432930
	ssh if3@authdns.k8x.in
	sleep 8
	SECONDS_FINAL=$SECONDS
	read -p "You took $SECONDS_FINAL seconds, do you want to add it to leaderboard? [Y/n]: " leadbool
	if [[ $leadbool != "n" ]]; then
		echo "1,$name,$SECONDS_FINAL" >> ./leaderboard-1.csv
	fi
elif [[ $challenge == "2" ]]; then
	echo "You have selected the Grafana/Prometheus setup challenge."
	echo "Provisioning VPS..."
	ssh if3@grafana.k8x.in "/usr/local/bin/reset-to-snapshot"
	sleep 5
	echo "Provisioned! Entering the Matrix..."
	echo "Please follow instruction booklet number 2"
	SECONDS=0 # bash builtin; see https://overflow.projectsegfau.lt/exchange/unix/questions/12068/how-to-measure-time-of-program-execution-and-store-that-inside-a-variable#432930
	ssh if3@grafana.k8x.in
	SECONDS_FINAL=$SECONDS
	read -p "You took $SECONDS_FINAL seconds, do you want to add it to leaderboard? [Y/n]: " leadbool
	if [[ $leadbool != "n" ]]; then
		echo "2,$name,$SECONDS_FINAL" >> ./leaderboard-2.csv
	fi
elif [[ $challenge == "3" ]]; then
	echo "You have selected the Invidious setup challenge."
	echo "Provisioning VPS..."
	ssh if3@invidious.k8x.in "/usr/local/bin/reset-to-snapshot"
	sleep 5
	echo "Provisioned! Entering the Matrix..."
	echo "Please follow instruction booklet number 3"
	SECONDS=0 # bash builtin; see https://overflow.projectsegfau.lt/exchange/unix/questions/12068/how-to-measure-time-of-program-execution-and-store-that-inside-a-variable#432930
	ssh if3@invidious.k8x.in
	SECONDS_FINAL=$SECONDS
	read -p "You took $SECONDS_FINAL seconds, do you want to add it to leaderboard? [Y/n]: " leadbool
	if [[ $leadbool != "n" ]]; then
		echo "3,$name,$SECONDS_FINAL" >> ./leaderboard-3.csv
	fi
elif [[ $challenge == "4" ]]; then
	echo "You have selected the XMPP Chat Server setup challenge."
	echo "Provisioning VPS..."
	ssh if3@xmpp.k8x.in "/usr/local/bin/reset-to-snapshot"
	sleep 5
	echo "Provisioned! Entering the Matrix..."
	echo "Please follow instruction booklet number 4"
	SECONDS=0 # bash builtin; see https://overflow.projectsegfau.lt/exchange/unix/questions/12068/how-to-measure-time-of-program-execution-and-store-that-inside-a-variable#432930
	ssh if3@xmpp.k8x.in
	SECONDS_FINAL=$SECONDS
	read -p "You took $SECONDS_FINAL seconds, do you want to add it to leaderboard? [Y/n]: " leadbool
	if [[ $leadbool != "n" ]]; then
		echo "4,$name, $SECONDS_FINAL" >> ./leaderboard-4.csv
	fi
else
	echo "Invalid Option Selected.."
fi
