echo "Selfhosting - Try It Yourself Leaderboard"
for i in {1..4}; do
	echo "-- Challenge $i --"
	while IFS="," read -r id name seconds; do
		echo "$seconds seconds - $name"
	done < leaderboard-$i.csv | sort
done
