# Self-Hosting Workshop

## What is self-hosting and why should you self-host?

Self-hosting refers to software installed & maintained by the user.

Self-hosting can vary from hosting on cloud servers to hosting locally using an old computer!

---

## Reasons to Self-Host
- More control over your digital belongings
- Gaining experience with system administration and DevOps
- Experience with debugging issues
- Quickly put up services when need be, for example during a hackathon
- Hosting personal services like your own website
- Data ownership 
- Usually aren't targeted much by attackers who go for bigger targets instead
- No need to pay a subscription to a SaaS company
- Control over how your data is handled and stored
- Burn free time :P

---

## About this workshop
- Split into two 2 hour sessions in the two days of the conference
- There is a try-it-yourself in the showcase section of the room where you can try setting up more complex services
- Each attendee gets a Digital Ocean VPS to hack on in the form of chits when they enter the room on first day. No replacement VPSes will be provided
- All of the VPSes run Debian 12 and are valid till Monday.

---

## Contents of the workshop
- Part 1 [1 hour 45 min]
  - `ssh`, `apt`, `tmux`
  - Static website deployment with Caddy
  - Deploying a firewall with `ufw`
  - Transferring files to the VPS with `rsync`
  - Monitoring resource usage with `htop` and phpsysinfo
- Part 2 [2 hours]
  - Dynamic website with python, caddy and systemd
  - Introduction to docker and docker-compose
  - Converting the previously created dynamic site to docker
  - Using Docker registries
  - Setting up an instance of a self-hosted password manager using docker

---

## Using SSH
- `ssh` or Secure SHell, is the utility used to remote into most modern servers.

- This utility is pre-installed on most linux and Mac systems, and windows computers running a Windows 10 version after ~2018.

To login using ssh, open your terminal/command prompt and run the following:

`ssh if3@HOSTNAME_IN_CHIT`

Note: Password prompt will not show placeholders for the password.

---

## Generating SSH keypair
- SSH is cryptographically secure, but password login is weak by design
- To solve this issue, private-public keypairs are used for log-in on most servers

To generate your ssh keypair, run the following in your terminal/command prompt:

`ssh-keygen -t ed25519`

You can use the defaults for almost all questions prompted.

Note: ED25519 is a more modern cryptographic standard and an alternative to the older RSA. It is preferred here due to its smaller key size.

The key will be stored in `USER_DIR/.ssh`, which is `/home/USERNAME/.ssh` on linux, `C:\Users\USERNAME\.ssh` and `/Users/USERNAME/.ssh` on mac. This directory also stores the configurations for the user.

---

## Making the server use your keypair

- Copy your public key contents. It will be in `USER_DIR/.ssh/id_ed25519.pub`
- Log back into the provisioned VPS with `ssh if3@HOSTNAME_IN_CHIT`
- Create a directory called `.ssh` with `mkdir ~/.ssh`
- Use the echo command to add the contents to authorized list of keys with `echo 'KEY_CONTENTS_FROM_CLIPBOARD' >> .ssh/authorized_keys`

At this point, when you log back in, you should be let in without a password, with the authorization being taken care of by the ssh key!

---

## Further securing of SSH

- Others can still login without key
- Root (superuser) login is enabled meaning its easier for bots to brute force the password.

To remedy this, we need to edit ssh server config.
For this, we use nano which is very easy to understand and use.

	nano /etc/ssh/sshd_config
	<navigate to PermitRootLogin section>
	PermitRootLogin no
	<navigate to PasswordAuthentication section>
	PasswordAuthentication no
	<navigate to UsePAM section>
	UsePAM no

---

## Using `apt`
- `apt` is the package manager used in Debian
- To fetch latest packages (not upgrade): `apt update`
- To upgrade system: `apt upgrade`
- To search for a package: `apt search <PACKAGE_NAME>`
- To get info about a package: `apt show <PACKAGE_NAME>`
- To install a package: `apt install <PACKAGE_NAME>`
- To remove a package: `apt remove <PACKAGE_NAME>`
- To remove a package and everything installed due to it: `apt purge <PACKAGE_NAME>`
- To remove un-needed dependencies: `apt autoremove`

---

## Using `tmux`
- `tmux` is a "terminal multiplexer" used for running multiple terminal sessions in a single window
- Can be used to multi-task from a single terminal window, or be used to persist long-running sessions over ssh (say a compilation or a long download)
- Launching a new session: `tmux`
- Attach to an existing `tmux` session: `tmux attach`
- Detaching from the session: `tmux detach`

---

## Useful `tmux` keybindings

- Main useful keybindings (followed by `Ctrl + B`)
  - `c`: Create a new window
  - `n`: Switch to the next window
  - `p`: Switch to the previous window
  - `d`: Detach from the session
  - `%`: Vertical Split
  - `"`: Horizontal Split
  - `o`: Cycle through panes
  - Scrollback & Clipboard
    - `[`: Toggle scrollback (Scroll with arrow keys, mouse or `hjkl`)
    - `Space`: Select a block of text to copy, terminate selection with `Enter`
    - `]`: Paste copied text

---

## Setting up Caddy

- Caddy is a web server and reverse proxy
- More modern than NginX and apache and is easier to configure
- Automatically handles HTTPS redirects and TLS certificates

---

## Installing caddy

Caddy isn't in debian repos, so we must add another package repository:

```
$ extrepo update
$ extrepo enable caddy
$ apt update
$ apt install caddy
```

- Enable the `systemd` service for automatically starting Caddy on every boot: `sudo systemctl enable caddy`

---

## Initial setup of caddy
- Write a basic configuration at `/etc/caddy/Caddyfile`:

```
HOSTNAME_IN_CHIT {
	respond "Hi!"
}
```

This configures Caddy to respond to all requests for `HOSTNAME_IN_CHIT` with a `Hi!` body, try opening the hostname given in the chit with your web browser!

---

## Configuring a Static Site with Caddy

Now that we have the core functionality working, we'll host our static site with Caddy

- Edit `/etc/caddy/Caddyfile` to point to the website's contents:

```diff=
 HOSTNAME_IN_CHIT {
-    respond "Hi!"
+    encode gzip
+
+    root * /var/www/html
+    file_server browse
 }
```

---

## Explanation of the changes

- A small breakdown of all the directives used:
  - `encode`: Compress the contents with the `gzip` algorithm, allowing faster page loads as less data is transmitted over the network
  - `root`, `file_server`: A file server for serving static files locate at the `root` directory (i.e. `/var/www/html`)
    - The `browse` directive under `file_server` is used to display a fancy directory listing with search included! This is useful for serving artifacts / binaries

- Reload the caddy config: `sudo caddy reload`

---

## Adding some content

In order to have some data to display, we can create a basic index.html file.

```shell=
$ mkdir -p /var/www/html
$ echo '<p>Hello World!</p>' > /var/www/html/index.html
```

That's it! Just reload the site in your browser (might need `Ctrl + Shift + R` to fetch a fresh copy) and see the changes.

If you want to see the `browse` mode in action, move the index.html to another name (such as index.html.bkp) and a file explorer interface will show up!

`mv /var/www/html/index.html /var/www/html/index.html.bkp`

---

## Setting up a firewall with UFW

- UFW (or Uncomplicated FireWall) is an easy to use firewall to control which ports are open and to which IPs
- Improves security (since internal ports cannot be exposed, even by mistake)
- Really powerful in controlling who can connect to the server and where they connect to

---

## Basic setup of UFW

- Install UFW: `apt install ufw`
- Start UFW on boot: `systemctl enable --now ufw`
- Setup basic rules so you don't get locked out:
  - Allow ssh traffic: `ufw allow 22/tcp`
  - Allow web traffic:
    - HTTP: `ufw allow 80/tcp`
    - HTTPS: `ufw allow 443/tcp`
    - QUIC/HTTP3: `ufw allow 443/udp`
- Enable UFW: `ufw enable`

---

## Transferring files to the VPS

We're going to set up a static site on the VPS now which involves transferring files from your local filesystem to the VPS. We'll be using the `rsync` command for this

- Install `rsync`: `sudo apt install rsync`
- Transfer the `html` directory to the VPS: `rsync -rtvzlP ./html root@host:/var/www/`
- For transferring a file FROM the VPS, we just have to flip the source and destination: `rsync -rtvzlP root@host:/var/www/html ./received_html`

Note that we can use `scp` as-well, but `rsync` provides us with various nice-to-haves such as resumable transfers

---

## Elaboration on the flags

- `-rtvzlP` can be expanded to `--recursive --times --verbose --compress --links --partial --progress`
  - The `--recursive` / `-r` flag tells `rsync` to copy all directory contents
  - The `--times` / `-t` flag tells `rsync` to copy over timestamps of files
  - The `--verbose` / `-v` flag tells `rsync` to give more info on current task.
  - The `--compress` / `-z` flag tells `rsync` to compress the files (reduces bandwidth usage) 
  - The `--links` / `-l` flag tells `rsync` to copy symlinks as-is.
    - Use `--copy-links` / `-L` if you want the data of linked file copied.
  - The `--partial --progress` / `-P` flag tells `rsync` to show stats on how much is copied, percentage etc.
    - Partial makes sure transfer can be resumed later if it gets cut mid-way

---

## Web monitoring with phpsysinfo
- PHPSysinfo allows you to see statistics on your server from a simple web UI.
- Since it exposes some sensitive info it needs to be behind password login.
- Install PHP Dependencies: `apt install php-fpm php-simplexml php-xml`
- Clone the PHPSysinfo Repo: `git clone https://github.com/phpsysinfo/phpsysinfo /var/www/phpsysinfo`
- Go to the PHPSysinfo directory and copy sample configuration file:
  - `cd /var/www/phpsysinfo`
  - `cp phpsysinfo.ini.new phpsysinfo.ini`
  - You can change `phpsysinfo.ini` as required, though no changes are needed by default

---

## Fronting PHPSysinfo with caddy
- A web server is needed to expose PHPSysinfo to the internet
- PHP uses a special type of backend called FastCGI which has to be proxied.
- To configure caddy to work with php, add the following to `/etc/caddy/Caddyfile`
```
phpsysinfo.HOSTNAME_IN_CHIT {
	php_fastcgi unix//run/php/php8.2-fpm.sock
	root * /var/www/phpsysinfo
	file_server
}
```

- Explanation:
  - `php_fastcgi`: a special directive to proxy .php files through FastCGI
  - `root` & `file_server`: to serve static files

Now, you can open phpsysinfo.HOSTNAME_IN_CHIT and you'll have a neat UI with system info!

---

## Setting up Password Authentication
- BasicAuth/HTTP Authentication is used in this case
- Can be setup in webserver itself
- Generate Password Hash: `caddy hash-password`
- Add configuration to Caddyfile:
```diff=
	file_server
+	basicauth {
+	    if3 HASH_GIVEN_BY_CADDY_HASHPASSWORD
+	}
}
```

In this case, if3 is username.

Now, when you open phpsysinfo.HOSTNAME_IN_CHIT, you should get a credentials pop-up before the data is shown!

---
