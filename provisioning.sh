for i in $(doctl compute droplet list --tag-name if3-selfhost -o json | jq -r '.[] | .networks.v4.[] | select(.type == "public")  | .ip_address'); do
	hostname="$(ssh -o 'StrictHostKeyChecking no' -o 'UserKnownHostsFile /dev/null' root@$i 'hostname')
	echo "doctl compute domain records create i10e.xyz --record-type A --record-name $hostname --record-data $i --record-ttl 300" >> ./dns-cmds.txt
	echo "doctl compute domain records create i10e.xyz --record-type A --record-name *.$hostname --record-data $i --record-ttl 300" >> ./dns-cmds.txt
	ssh -o "StrictHostKeyChecking no" -o "UserKnownHostsFile /dev/null" root@$i 'bash -c "apt -y install sudo extrepo nano;
	groupadd --gid 1006 if3;
	adduser --gecos \"\" --disabled-password --uid 1006 --gid 1006 --shell /bin/bash --home /home/if3 if3;
	echo if3:IndiaFOSS_SelfHost | chpasswd;
	usermod -aG sudo if3;
	sed -i \"s/^PermitRootLogin.*/PermitRootLogin no/g\" /etc/ssh/sshd_config;
	sed -i \"s/^PasswordAuthentication.*/PasswordAuthentication yes/g\" /etc/ssh/sshd_config;
	sed -i \"s/^KbdInteractiveAuthentication.*/KbdInteractiveAuthentication yes/g\" /etc/ssh/sshd_config;
	sed -i \"s/^%sudo.*/%sudo ALL=(ALL) NOPASSWD: ALL/g\" /etc/sudoers;
	systemctl restart sshd;"'
done
